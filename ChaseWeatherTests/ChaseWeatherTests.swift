//
//  ChaseWeatherTests.swift
//  ChaseWeatherTests
//
//  Created by Hyewook Song on 5/16/23.
//

import XCTest
@testable import ChaseWeather

final class ChaseWeatherViewModelTests: XCTestCase {
    
    var viewModel: WeatherViewModel!
    var fakeCurrent: Current!
    var fakeForecast: Forecast!

    override func setUpWithError() throws {
        viewModel = WeatherViewModel()
        let fakeMain = MainData(temp: 100, feels_like: 50, temp_min: 20, temp_max: 120)
        let fakeWeather = WeatherData(id: 0, main: "test", description: "test", icon: "test")
        fakeCurrent = Current(name: "test", main: fakeMain, weather: [fakeWeather])
        let fakeCity = CityData(name: "test")
        let fakeDay = Day(dt: 0, main: fakeMain, weather: [fakeWeather])
        fakeForecast = Forecast(city: fakeCity, list: [fakeDay])
    }

    override func tearDownWithError() throws {
        viewModel = nil
        fakeCurrent = nil
        fakeForecast = nil
    }

    func testCurrentBind() {
        viewModel.bindCurrentWeather = { weather in
            
            XCTAssertEqual(weather?.name, "test")
            XCTAssertEqual(weather?.main.temp, 100)
            XCTAssertNotEqual(weather?.weather.first?.description, "fake")
        }
        
        viewModel.currentWeather = fakeCurrent
    }

    func testForecastBind() {
        viewModel.bindForecastWeather = { weather in
            
            XCTAssertEqual(weather?.city.name, "test")
            XCTAssertEqual(weather?.list.first?.dt, 0)
            XCTAssertNotEqual(weather?.list.first?.weather.first?.id, 100)
        }
        
        viewModel.forecastWeather = fakeForecast
    }
    
    func testEpochDay() {
        XCTAssertEqual(WeatherViewModel.getDayFromEpoch(date: 1684556083), "Friday")
    }
}
