//
//  Weather.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/16/23.
//

import Foundation

public struct WeatherData: Codable {
    var id: Int
    var main: String
    var description: String
    var icon: String
}

public struct MainData: Codable {
    var temp: Double
    var feels_like: Double
    var temp_min: Double
    var temp_max: Double
}

public struct CityData: Codable {
    var name: String
}

public struct Day: Codable {
    var dt: Double
    var main: MainData
    var weather: [WeatherData]
}

public struct Forecast: Codable {
    var city: CityData
    var list: [Day]
}

public struct Current: Codable {
    var name: String
    var main: MainData
    var weather: [WeatherData]
}

public struct Location: Codable {
    var lat: Double
    var lon: Double
}
