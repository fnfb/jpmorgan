//
//  Keys.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/18/23.
//

import Foundation

struct UserDefaultsKeys {
    static let userlat = "lat"
    static let userlon = "lon"
}
