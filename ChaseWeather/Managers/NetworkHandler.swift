//
//  NetworkHandler.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/16/23.
//

import Foundation

class NetworkIngHandler {
    enum NetworkError: Error {
        case invalidAuth
        case invalidURL
        case invalidData
    }
    
    static func fetchCurrentWeather(lat: Double, lon: Double) async throws -> Current {
        guard let APIkey = Bundle.main.infoDictionary?["APIkey"]  as? String else {
            throw NetworkError.invalidAuth
        }
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=" + String(lat) + "&lon=" + String(lon) + "&appid=" + APIkey) else {
            throw NetworkError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw NetworkError.invalidData
        }

        let decoder = JSONDecoder()
        return try decoder.decode(Current.self, from: data)
    }

    static func fetchForecastWeather(lat: Double, lon: Double) async throws -> Forecast {
        guard let APIkey = Bundle.main.infoDictionary?["APIkey"]  as? String else {
            throw NetworkError.invalidAuth
        }
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?lat=" + String(lat) + "&lon=" + String(lon) + "&appid=" + APIkey) else {
            throw NetworkError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw NetworkError.invalidData
        }

        let decoder = JSONDecoder()
        return try decoder.decode(Forecast.self, from: data)
    }
    
    static func weatherIconURL(_ icon: String) -> URL? {
        return URL(string: "https://openweathermap.org/img/wn/" + icon + "@2x.png")
    }
    
    static func fetchLocation(city: String) async throws -> [Location] {
        guard let APIkey = Bundle.main.infoDictionary?["APIkey"] as? String else {
            throw NetworkError.invalidAuth
        }
        guard let cityString = city.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let url = URL(string: "https://api.openweathermap.org/geo/1.0/direct?q=" + cityString + ",US&appid=" + APIkey) else {
            throw NetworkError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw NetworkError.invalidData
        }
        
        let decoder = JSONDecoder()
        return try decoder.decode([Location].self, from: data)
    }
}
