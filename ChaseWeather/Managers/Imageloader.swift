//
//  Imageloader.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/17/23.
//

import UIKit

public class ImageLoader {
    static let shared = ImageLoader()
    private let cache = NSCache<NSString, UIImage>()
    
    public func loadImage(fromURL url: URL, completion: @escaping (UIImage?) -> Void) -> URLSessionTask {
        let cacheKey = NSString(string: url.absoluteString)
        if let image  = cache.object(forKey: cacheKey) {
            completion(image)
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            let result: UIImage?
            
            guard error == nil else {
                print("Image fetch returned error")
                return
            }
            
            guard let data = data else {
                print("Image fetch failed")
                completion(nil)
                return
            }
            
            result = UIImage(data: data)
            if let image = result {
                self.cache.setObject(image, forKey: cacheKey)
            }
            completion(result)
        }
        task.resume()
        
        return task
    }
}
