//
//  LocationManager.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/18/23.
//

import Foundation
import CoreLocation

extension Notification.Name {
    static let locationDidChangeNotification = Notification.Name("LocationDidChangeNotification")
}

public class LocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = LocationManager()
    
    private var locationManager = CLLocationManager()
    
    private override init () {
        super.init()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            UserDefaults.standard.set(location.coordinate.latitude, forKey: UserDefaultsKeys.userlat)
            UserDefaults.standard.set(location.coordinate.longitude, forKey: UserDefaultsKeys.userlon)

            NotificationCenter.default.post(name: .locationDidChangeNotification, object: nil)
        }
    }
}
