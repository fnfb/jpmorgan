//
//  WeatherViewModel.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/17/23.
//

import Foundation

public class WeatherViewModel {
    static let margin: CGFloat = 10
    static let imageSize: CGFloat = 80

    var currentWeather: Current? {
        didSet {
            bindCurrentWeather?(currentWeather)
        }
    }
    
    var forecastWeather: Forecast? {
        didSet {
            bindForecastWeather?(forecastWeather)
        }
    }
    
    private var userLat: Double? {
        get {
            return UserDefaults.standard.value(forKey: UserDefaultsKeys.userlat) as? Double
        }
    }
    
    private var userLon: Double? {
        get {
            return UserDefaults.standard.value(forKey: UserDefaultsKeys.userlon) as? Double
        }
    }
    
    var bindCurrentWeather: ((Current?) -> Void)?
    
    var bindForecastWeather: ((Forecast?) -> Void)?
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(locationUpdated), name: .locationDidChangeNotification, object: nil)
    }
    
    @objc private func locationUpdated() {
        fetchCurrentWeather()
        fetchForecastWeather()
    }
    
    func fetchLocationFromSearch(city: String) {
        Task {
            do {
                let location = try await NetworkIngHandler.fetchLocation(city: city)
                if let userLocation = location.first {
                    UserDefaults.standard.set(userLocation.lat, forKey: UserDefaultsKeys.userlat)
                    UserDefaults.standard.set(userLocation.lon, forKey: UserDefaultsKeys.userlon)
                    NotificationCenter.default.post(name: .locationDidChangeNotification, object: nil)
                } else {
                    // TODO: Display error alert
                    print("No Location found")
                }
            } catch {
                if let error = error as? NetworkIngHandler.NetworkError {
                    switch error {
                    case .invalidURL:
                        print("Invalid URL")
                    case .invalidData:
                        print("Invalid Data")
                    case .invalidAuth:
                        print("Invalid API key")
                    }
                }
            }
        }
    }
    
    func fetchCurrentWeather() {
        guard let lat = userLat, let lon = userLon else { return }
        Task {
            do {
                currentWeather = try await NetworkIngHandler.fetchCurrentWeather(lat: lat, lon: lon)
            } catch {
                if let error = error as? NetworkIngHandler.NetworkError {
                    switch error {
                    case .invalidURL:
                        print("Invalid URL")
                    case .invalidData:
                        print("Invalid Data")
                    case .invalidAuth:
                        print("Invalid API key")
                    }
                }
            }
        }
    }
    
    func fetchForecastWeather() {
        guard let lat = userLat, let lon = userLon else { return }
        Task {
            do {
                forecastWeather = try await NetworkIngHandler.fetchForecastWeather(lat: lat, lon: lon)
            } catch {
                if let error = error as? NetworkIngHandler.NetworkError {
                    switch error {
                    case .invalidURL:
                        print("Invalid URL")
                    case .invalidData:
                        print("Invalid Data")
                    case .invalidAuth:
                        print("Invalid API key")
                    }
                }
            }
        }
    }
    
    static func getDayFromEpoch(date: Double) -> String {
        let date = NSDate(timeIntervalSince1970: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "cccc"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: date as Date)
    }
}
