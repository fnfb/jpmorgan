//
//  ForecastView.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/17/23.
//

import UIKit

class ForecastView: UITableView {
    private let forecastCellReuseIdentifier = "ForecastCell"
    
    private var viewModel: WeatherViewModel?
    private var forecastList: [Day] = [] {
        didSet {
            guard forecastList.count != 0 else { return }
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        register(ForecastCell.self, forCellReuseIdentifier: forecastCellReuseIdentifier)
        dataSource = self
        delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(viewModel: WeatherViewModel) {
        self.viewModel = viewModel
    }
    
    func fetchForecastWeather() {
        viewModel?.bindForecastWeather = { [weak self] weather in
            guard let weather = weather else {
                print("No Data")
                return
            }
            DispatchQueue.main.async {
                self?.forecastList = weather.list
            }
        }
        viewModel?.fetchForecastWeather()
    }
}

extension ForecastView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: forecastCellReuseIdentifier, for: indexPath) as? ForecastCell {
            cell.set(day: forecastList[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}
