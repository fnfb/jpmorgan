//
//  ForecastCell.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/17/23.
//

import UIKit

class ForecastCell: UITableViewCell {
    private var day: Day? {
        didSet {
            updateView()
        }
    }
    private let stackView = UIStackView()
    private let dateLabel = UILabel()
    private var iconImageView = UIImageView()
    private let minLabel = UILabel()
    private let maxLabel = UILabel()
    
    var imageFetchingTask: URLSessionTask? {
        didSet {
            guard imageFetchingTask != oldValue else { return }
            
            oldValue?.cancel()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        day = nil
        iconImageView.image = nil
    }
    
    public func set(day: Day) {
        self.day = day
    }
    
    private func setupView() {
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        stackView.contentMode = .left
        stackView.spacing = WeatherViewModel.margin
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        dateLabel.numberOfLines = 1
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(dateLabel)
        
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(iconImageView)
        
        minLabel.numberOfLines = 1
        minLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(minLabel)
        
        maxLabel.numberOfLines = 1
        maxLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(maxLabel)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
            iconImageView.heightAnchor.constraint(equalToConstant: WeatherViewModel.imageSize),
            iconImageView.widthAnchor.constraint(equalToConstant: WeatherViewModel.imageSize)
        ])
    }
    
    private func updateView() {
        guard let day = day else { return }
        dateLabel.text = WeatherViewModel.getDayFromEpoch(date: day.dt)
        minLabel.text = String(day.main.temp_min)
        maxLabel.text = String(day.main.temp_max)
        if let weather = day.weather.first, let iconURL = NetworkIngHandler.weatherIconURL(weather.icon) {
            self.imageFetchingTask = ImageLoader.shared.loadImage(fromURL: iconURL, completion: { [weak self] image in
                DispatchQueue.main.async {
                    self?.iconImageView.image = image
                }
            })
        }
    }
}
