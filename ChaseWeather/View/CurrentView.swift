//
//  CurrentView.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/17/23.
//

import UIKit

class CurrentView: UIView {
    private let cityLabel = UILabel()
    private let tempLabel = UILabel()
    private let descLabel = UILabel()
    private let iconImage = UIImageView()
    let stackView = UIStackView()

    private var viewModel: WeatherViewModel?
    private var imageFetchingTask: URLSessionTask? {
        didSet {
            guard imageFetchingTask != oldValue else { return }
            oldValue?.cancel()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.contentMode = .scaleAspectFit
        stackView.spacing = WeatherViewModel.margin
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        cityLabel.numberOfLines = 0
        stackView.addArrangedSubview(cityLabel)
        
        tempLabel.numberOfLines = 1
        stackView.addArrangedSubview(tempLabel)
        
        descLabel.numberOfLines = 1
        stackView.addArrangedSubview(descLabel)

        iconImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(iconImage)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: WeatherViewModel.margin * 2),
            stackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),

            iconImage.centerYAnchor.constraint(equalTo: stackView.centerYAnchor),
            iconImage.heightAnchor.constraint(equalToConstant: WeatherViewModel.imageSize),
            iconImage.widthAnchor.constraint(equalToConstant: WeatherViewModel.imageSize),
            iconImage.trailingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: -WeatherViewModel.margin)
        ])
    }
    
    func set(viewModel: WeatherViewModel) {
        self.viewModel = viewModel
    }

    func fetchCurrentWeather() {
        viewModel?.bindCurrentWeather = { [weak self] weather in
            guard let weather = weather else {
                print("No Data received")
                return
            }
            DispatchQueue.main.async {
                self?.updateView(weather)
            }
        }
        viewModel?.fetchCurrentWeather()
    }
    
    func updateView(_ weather: Current) {
        cityLabel.text = weather.name
        tempLabel.text = String(weather.main.temp)
        if let description = weather.weather.first?.description {
            descLabel.text = description
        }
        if let weatherData = weather.weather.first, let iconURL = NetworkIngHandler.weatherIconURL(weatherData.icon) {
            self.imageFetchingTask = ImageLoader.shared.loadImage(fromURL: iconURL, completion: { [weak self] image in
                DispatchQueue.main.async {
                    self?.iconImage.image = image
                }
            })
        }
    }
}
