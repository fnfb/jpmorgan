//
//  ViewController.swift
//  ChaseWeather
//
//  Created by Hyewook Song on 5/16/23.
//

import UIKit

class ViewController: UIViewController {
    private let currentView = CurrentView()
    private let forecastView = ForecastView()
    private let searchBar = UISearchBar()
    private let viewModel = WeatherViewModel()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        searchBar.delegate = self
        searchBar.placeholder = "Search City"
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        currentView.set(viewModel: viewModel)
        forecastView.set(viewModel: viewModel)
        currentView.fetchCurrentWeather()
        forecastView.fetchForecastWeather()
    }

    private func setupView() {
        view.translatesAutoresizingMaskIntoConstraints = false
        currentView.translatesAutoresizingMaskIntoConstraints = false
        forecastView.translatesAutoresizingMaskIntoConstraints = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(currentView)
        view.addSubview(forecastView)
        view.addSubview(searchBar)

        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            searchBar.bottomAnchor.constraint(equalTo: currentView.topAnchor),

            currentView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            currentView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            currentView.bottomAnchor.constraint(equalTo: forecastView.topAnchor),

            forecastView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            forecastView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            forecastView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
        ])
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text {
            viewModel.fetchLocationFromSearch(city: searchText)
        }
        searchBar.resignFirstResponder()
    }
}
